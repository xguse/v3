def SS2(handle):
    from api.files import get_lines_of_file

    SS2_datastructure = {
            'aa_sequence': "",
            'structure_sequence': "",
            'letter_annotations': {}
    }

    content_start_line = 2

    file_lines = get_lines_of_file(handle)
    file_lines = file_lines[content_start_line:]

    return reduce(lambda structure, data:
            append_data_to_ss2_datastructure(structure, data),
            file_lines, SS2_datastructure)


def append_data_to_ss2_datastructure(ss2_structure, data):
    from copy import deepcopy

    SS2_datastructure = deepcopy(ss2_structure)

    position, aa_letter, struct_letter, \
            first_index, second_index, third_index = _extract_data_from_register(data)

    SS2_datastructure['aa_sequence'] += aa_letter
    SS2_datastructure['structure_sequence'] += struct_letter

    SS2_datastructure['letter_annotations'][position] = {
                aa_letter : \
                    { struct_letter: [first_index, second_index, third_index] }
            }

    return SS2_datastructure


def _extract_data_from_register(register):
    fields = {
            'position': 0,
            'aminoacid_letter': 1,
            'structure_letter': 2,
            'first_unknown_index': 3,
            'second_unknown_index': 4,
            'third_unknown_index': 5,
    }

    register_values = register.split()

    position = register_values[fields['position']]

    aa_letter = register_values[fields['aminoacid_letter']]
    struct_letter = register_values[fields['structure_letter']]

    first_index = register_values[fields['first_unknown_index']]
    second_index = register_values[fields['second_unknown_index']]
    third_index = register_values[fields['third_unknown_index']]

    return int(position), aa_letter, struct_letter, float(first_index), float(second_index), float(third_index)
