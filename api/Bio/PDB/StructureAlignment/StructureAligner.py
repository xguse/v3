"""Class for implement wrappers for structure aligners"""
class StructureAligner(object):
    def __init__(self):
        pass

    def align(pdb_a, pdb_b):
        raise NameError('Method for align structures not implement in this object!')

    def parse_output_from_alignment(alignment):
        raise NameError('method not implemented in this class!')
