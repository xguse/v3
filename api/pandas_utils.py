def load_csv_dataset(csv_dataset_filename):
    import pandas as pd
    dataset = pd.read_csv(csv_dataset_filename, header = 0)
    return dataset

def features_and_classes_from(dataset):
    dataset_copy = dataset.copy()
    class_ = dataset['class']

    del dataset_copy['class']

    return dataset_copy, class_

def column_to_list_converter(dataset):
    def convert(column):
        return dataset[column].as_matrix().tolist()

    return convert
