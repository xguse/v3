from api.graph.representative_selection import most_central_node, select_representatives


def test_most_central_node():
    print "testing most_central_node..."
    from networkx import Graph

    G = Graph()
    G.add_edge(1, 2)
    G.add_edge(1, 3)
    G.add_edge(1, 4)
    G.add_edge(3, 4)

    assert most_central_node(G) == 1


def test_select_representatives():
    print "testing select representatives..."
    from networkx import Graph

    G = Graph()
    G.add_edge(1, 2)
    G.add_edge(1, 3)
    G.add_edge(1, 4)
    G.add_edge(6, 7)

    representatives = select_representatives(G)

    assert 1 in representatives
    assert 6 in representatives or 7 in representatives

    G = Graph()

    n = 10
    for i in range(1, n):
        G.add_edge(i, i+1)

    representatives = select_representatives(G)
    print representatives

    assert len(representatives) <= n / 2

    G = Graph()
    G.add_edge(3, 2)
    G.add_edge(1, 2)
    G.add_edge(2, 3)

    representatives = select_representatives(G)

    assert len(representatives) == 1

    G = Graph()

    n = 10
    for i in range(1, n+1):
        G.add_node(i)

    G.add_edge(1, 2)

    representatives = select_representatives(G)

    assert len(representatives) >= n - 1


if __name__ == '__main__':
    test_most_central_node()
    test_select_representatives()

    print "Tests passed!"
