def most_central_node(graph):
    most_central = None
    nodes = graph.nodes()
    for node in nodes:
        if most_central is None:
            most_central = node
        elif len(graph[node]) > len(graph[most_central]):
            most_central = node

    return most_central


def recolor_nodes(node_color, centroid, graph):
    node_color[centroid] = 'gray'

    for node in graph[centroid]:
        node_color[node] = 'gray'

    return node_color

'''
name: select_representatives
descr: given a [networkx] graph, selects the nodes by which
       it's possible to reach all of the other nodes in graph
input : G (a networkx Graph)
output: representatives a list of nodes
'''
def select_representatives(graph):
    nodes = graph.nodes()

    node_color = {node: 'white' for node in nodes}

    number_of_non_reached_nodes = len(nodes)

    representatives = []

    while number_of_non_reached_nodes > 0:
        centroid = most_central_node(graph)
        representatives.append(centroid)

        node_color = recolor_nodes(node_color, centroid, graph)

        non_reached_nodes = filter(lambda el: node_color[el] == 'white', nodes)
        number_of_non_reached_nodes = len(non_reached_nodes)

        graph.remove_node(centroid)
        nodes = graph.nodes()

    return representatives
