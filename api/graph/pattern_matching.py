"""
name: build_graph_based_on
descr: Builds a graph based on a set of patterns (strings)
input: patterns (list of string)
output: graph (networkx graph)
"""
def build_graph_based_on(patterns):
    from networkx import DiGraph
    graph = DiGraph()

    for pattern in patterns:
        num_chars = len(pattern)

        weight = 1
        for i in range(num_chars-1):
            char_i = pattern[i]
            char_j = pattern[i+1]

            if char_i == char_j:
                weight += 1
            elif graph.has_edge(char_i, char_j) and weight > graph[char_i][char_j]['weight']:
                    graph[char_i][char_j]['weight'] = weight
            else:
                graph.add_edge(char_i, char_j)
                graph[char_i][char_j]['weight'] = weight

                weight = 1

    return graph

def _max_weight_from(node,graph):
    if node not in graph.nodes():
        return 0

    neighbors = graph.neighbors(node)

    max_weight = graph[node][neighbors[0]]['weight']

    return max(graph[node][neighbor]['weight'] for neighbor in graph.neighbors(node))

"""
name: patterns_in
descr: given a sequence and a list of patterns, return the list of patterns
       from 'patterns' present in sequence
input: sequence (string)
       patterns (list of string]
output: patterns (list of string)
"""
def patterns_in(sequence, patterns):
    num_chars = len(sequence)

    pattern_graph = build_graph_based_on(patterns)

    weight = 1
    pattern = sequence[0]
    patterns_list = []
    for i in range(1,num_chars):
        char_i = sequence[i]
        char_j = sequence[i-1]

        if char_i == char_j:
            weight += 1

            max_repetitions = _max_weight_from(char_i, pattern_graph)

            if weight <= max_repetitions:
                pattern += char_i
            else:
                patterns_list.append(pattern)
                pattern = char_i
                weight = 1
        elif pattern_graph.has_edge(char_j, char_i):
            max_weight = pattern_graph[char_j][char_i]['weight']

            if weight <= max_weight:
                pattern += char_i
            else:
                patterns_list.append(pattern)
                pattern = char_i
                weight = 1
        else:
            pattern, weight = char_i, 1

    if len(pattern) > 1:
        patterns_list.append(pattern)

    return patterns_list
