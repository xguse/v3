# Notes about this api

## Suffix Tree code
Modules about Suffix Tree were taken from [1]. I have slight modificated
LCS algorithm so that instead of return longest substring, it does all
the longests substrings

## Bio package
This package is an attempt to extend Biopython features, including new ones
that fit for my research. Up to current date, this package has wrappers for
three bioinformatics softwares: PSIPRED [2], CE [3], and Mammoth [4].

PSIPRED itself is included in its respective wrapper, however this is not
the case for CE and MAMMOTH.

### Instructions for using wrappers

1. First download and install CE and Mammoth;
2. Change line 71 of file `api/Bio/PDB/StructureAlignment/CE.py` to directory in which CE is installed;
3. Change line 13 of file `api/Bio/PDB/StructureAlignment/mammoth.py` to directory in which mammoth is installed;
4. Change line 2 of file `api/Bio/PSIPRED/psipred.py` to absolute version of this directory.
 
## References

[1] https://github.com/ptrus/suffix-trees
[2] http://bioinf.cs.ucl.ac.uk/psipred/
[3] http://source.rcsb.org/ceHome.jsp
[4] https://ub.cbm.uam.es/software/mammoth.php

## Academic references for softwares wrapped

#### PSIPRED
Jones DT. (1999) Protein secondary structure prediction based on position-specific scoring matrices. J. Mol. Biol. 292: 195-202. 

#### CE
Shindyalov, Ilya N., and Philip E. Bourne. "Protein structure alignment by incremental combinatorial extension (CE) of the optimal path." Protein engineering 11.9 (1998): 739-747.

#### Mammoth
Ortiz, Angel R., Charlie EM Strauss, and Osvaldo Olmea. "MAMMOTH (matching molecular models obtained from theory): an automated method for model comparison." Protein Science 11.11 (2002): 2606-2621.
