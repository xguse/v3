def f(x): return 2*x

def g(x): return x + 1

def h(x): return x ** 3

if __name__ == '__main__':
    from api.pipeline import pipeline

    math_pipeline = pipeline(f,g,h)

    print math_pipeline(4)

    assert(math_pipeline(2)) == 125
    assert(math_pipeline(0)) == 1
    assert(math_pipeline(1)) == 27
